package com.yh.dao;

import com.yh.javabean.Admin;

import java.util.ArrayList;

public interface AdminDao {

    ArrayList<Admin> selectByUsernameAndPassword(String username, String password);

    int insert(Admin admin);

    int deleteById(int id);

    int updatePasswordById(int id, String password);
}
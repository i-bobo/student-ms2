package com.yh.dao;



import com.yh.javabean.Teacher;

import java.util.ArrayList;

public interface TeacherDao {
    ArrayList<Teacher> selectByUsernameAndPassword(String username, String password);

    ArrayList<Teacher> selectAll();

    int updatePasswordByTid(int tid,String password);

    int insert(Teacher teacher);

    int deleteByTid(int tid);
}

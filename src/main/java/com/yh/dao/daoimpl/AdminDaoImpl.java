package com.yh.dao.daoimpl;
import com.yh.dao.AdminDao;
import com.yh.javabean.Admin;
import com.yh.util.MyJdbcUtil;

import java.util.ArrayList;

public class AdminDaoImpl implements AdminDao {
    @Override
    public ArrayList<Admin> selectByUsernameAndPassword(String username, String password) {
        String sql = "select * from admin where username = ? and password = ?";
        return MyJdbcUtil.dql(Admin.class,sql,username,password);
    }

    @Override
    public int insert(Admin admin) {
        String sql = "insert into admin (username,password,`desc`) values (?,?,?)";
        return MyJdbcUtil.dml(sql,admin.getUsername(),admin.getPassword(),admin.getDesc());
    }

    @Override
    public int deleteById(int id) {
        String sql = "delete from admin where id = ?";
        return MyJdbcUtil.dml(sql,id);
    }

    @Override
    public int updatePasswordById(int id, String password) {
        String sql = "update admin set password = ? where id = ?";
        return MyJdbcUtil.dml(sql,password,2);
    }
}

package com.yh.dao.daoimpl;

import com.yh.dao.StudentDao;
import com.yh.javabean.Student;
import com.yh.util.MyJdbcUtil;

import java.util.ArrayList;

public class StudentDaoImpl implements StudentDao {

    @Override
    public ArrayList<Student> selectByUsernameAndPassword(String username, String password) {
        String sql = "select * from student where username = ? and password = ?";
        return MyJdbcUtil.dql(Student.class,sql,username,password);
    }

    @Override
    public int updateTidBySid(int sid, int tid) {
        String sql = "update student set tid = ? where sid = ?";
        return MyJdbcUtil.dml(sql,tid,sid);
    }

    @Override
    public ArrayList<Student> selectBySid(int sid) {
        String sql = "select * from student where sid = ?";
        return MyJdbcUtil.dql(Student.class,sql,sid);
    }

    @Override
    public int updatePasswordBySid(int sid, String password) {
        String sql = "update student set password = ? where sid = ?";
        return MyJdbcUtil.dml(sql,password,sid);
    }

    @Override
    public ArrayList<Student> selectByTid(int tid) {
        String sql = "select * from student where tid = ?";
        return MyJdbcUtil.dql(Student.class,sql,tid);
    }

    @Override
    public ArrayList<Student> selectByTidAndStatus(int tid, int status) {
        String sql = "select * from student where tid = ? and status = ?";
        return MyJdbcUtil.dql(Student.class,sql,tid,status);
    }

    @Override
    public int updateStatusByTid(int tid, int status) {
        String sql = "update student set status = ? where tid = ?";
        return MyJdbcUtil.dml(sql,status,tid);
    }

    @Override
    public int updateStatusBySid(int sid, int status) {
        String sql = "update student set status = ? where sid = ?";
        return MyJdbcUtil.dml(sql,status,sid);
    }

    @Override
    public int updateScoreBySid(int sid, double score) {
        String sql = "update student set score = ? where sid = ?";
        return MyJdbcUtil.dml(sql,score,sid);
    }

    @Override
    public int insert(Student student) {
        String sql = "insert into student (username,password,`name`,tid,score,`status`) values (?,?,?,?,?,?)";
        return MyJdbcUtil.dml(sql,student.getUsername(),student.getPassword(),student.getName(),student.getTid(),student.getScore(),student.getStatus());
    }

    @Override
    public int deleteBySid(int sid) {
        String sql = "delete from student where sid = ?";
        return MyJdbcUtil.dml(sql,sid);
    }

}

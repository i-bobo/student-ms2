package com.yh.dao.daoimpl;


import com.yh.dao.TeacherDao;
import com.yh.javabean.Teacher;
import com.yh.util.MyJdbcUtil;

import java.util.ArrayList;

public class TeacherDaoImpl implements TeacherDao {
    @Override
    public ArrayList<Teacher> selectByUsernameAndPassword(String username, String password) {
        String sql = "select * from teacher where username = ? and password = ?";
        return MyJdbcUtil.dql(Teacher.class,sql,username,password);
    }

    @Override
    public ArrayList<Teacher> selectAll() {
        String sql = "select * from teacher";
        return MyJdbcUtil.dql(Teacher.class,sql);
    }

    @Override
    public int updatePasswordByTid(int tid, String password) {
        String sql = "update teacher set password = ? where tid = ?";
        return MyJdbcUtil.dml(sql,password,tid);
    }

    @Override
    public int insert(Teacher teacher) {
        String sql = "insert into teacher (username,password,tname,tdesc) values (?,?,?,?)";
        return MyJdbcUtil.dml(sql,teacher.getUsername(),teacher.getPassword(),teacher.getTname(),teacher.getTdesc());
    }

    @Override
    public int deleteByTid(int tid) {
        String sql = "delete from teacher where tid = ?";
        return MyJdbcUtil.dml(sql,tid);
    }
}

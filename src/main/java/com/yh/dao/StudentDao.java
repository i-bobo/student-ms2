package com.yh.dao;


import com.yh.javabean.Student;

import java.util.ArrayList;

public interface StudentDao {
    ArrayList<Student> selectByUsernameAndPassword(String username, String password);

    int updateTidBySid(int sid,int tid);


    ArrayList<Student> selectBySid(int sid);

    int updatePasswordBySid(int sid,String password);

    ArrayList<Student> selectByTid(int tid);

    ArrayList<Student> selectByTidAndStatus(int tid,int status);

    int updateStatusByTid(int tid,int status);

    int updateStatusBySid(int sid,int status);

    int updateScoreBySid(int sid,double score);

    int insert(Student student);

    int deleteBySid(int sid);

//    int updatePasswordBySid(int sid,String password);
}

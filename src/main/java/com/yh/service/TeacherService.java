package com.yh.service;

import com.yh.javabean.Student;

import java.util.ArrayList;

public interface TeacherService {
    ArrayList<Student> showMyStudent(int tid);

    void upLoadScore(int sid,double score);

    String unluckyWretch(int tid);

    void updateStatus(int sid,int status);

    void restPassword(int tid,String password);
}

package com.yh.service;

import com.yh.javabean.Admin;
import com.yh.javabean.Student;
import com.yh.javabean.Teacher;

public interface MainService {

    Student studentLogin(String username,String password);

    Teacher teacherLogin(String username,String password);

    Admin AdminLogin(String username,String password);
}

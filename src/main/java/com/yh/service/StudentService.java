package com.yh.service;

import com.yh.javabean.Student;
import com.yh.javabean.Teacher;

import java.util.ArrayList;

public interface StudentService {

    ArrayList<Teacher> showAllTeacherInfo();

    void selectTeacher(int sid,int tid);

    Student MyInfo(int sid);

    void restPassword(int sid,String password);
}

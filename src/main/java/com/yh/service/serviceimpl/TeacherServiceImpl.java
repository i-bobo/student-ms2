package com.yh.service.serviceimpl;

import com.yh.dao.StudentDao;
import com.yh.dao.TeacherDao;

import com.yh.dao.daoimpl.StudentDaoImpl;
import com.yh.dao.daoimpl.TeacherDaoImpl;
import com.yh.javabean.Student;
import com.yh.service.TeacherService;

import java.util.ArrayList;
import java.util.Random;

public class TeacherServiceImpl implements TeacherService {
    StudentDao studentDao =  new StudentDaoImpl();
    TeacherDao teacherDao = new TeacherDaoImpl();
    @Override
    public ArrayList<Student> showMyStudent(int tid) {
        ArrayList<Student> students = studentDao.selectByTid(tid);
        return students;
    }

    @Override
    public void upLoadScore(int sid, double score) {
        studentDao.updateScoreBySid(sid, score);
    }

    @Override
    public String unluckyWretch(int tid) {
        ArrayList<Student> students = studentDao.selectByTidAndStatus(tid, 0);

        Random random = new Random();
        if (students.size() == 0){
            return "没有学生";
        }else{
            int index = random.nextInt(students.size());
            Student student = students.get(index);
            int sid = student.getSid();
            System.out.println(sid);
            studentDao.updateStatusBySid(sid,1);
            if(students.size()==1){
                studentDao.updateStatusByTid(tid,0);
            }
            return student.getName();
        }
    }

    @Override
    public void updateStatus(int sid, int status) {
        studentDao.updateStatusBySid(sid, status);
    }

    @Override
    public void restPassword(int tid, String password) {
       teacherDao.updatePasswordByTid(tid,password);
    }
}

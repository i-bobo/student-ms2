package com.yh.service.serviceimpl;

import com.yh.dao.StudentDao;

import com.yh.dao.daoimpl.StudentDaoImpl;
import com.yh.dao.daoimpl.TeacherDaoImpl;
import com.yh.javabean.Student;
import com.yh.javabean.Teacher;
import com.yh.service.StudentService;

import java.util.ArrayList;

public class StudentServiceImpl implements StudentService {
    StudentDao studentDao =  new StudentDaoImpl();
    TeacherDaoImpl teacherDao = new TeacherDaoImpl();

    @Override
    public ArrayList<Teacher> showAllTeacherInfo() {
        return teacherDao.selectAll();
    }

    @Override
    public void selectTeacher(int sid, int tid) {
        studentDao.updateTidBySid(sid,tid);
    }

    @Override
    public Student MyInfo(int sid) {
        ArrayList<Student> students = studentDao.selectBySid(sid);
        if(students.isEmpty()){
            return null;
        }
        return students.get(0);
    }


    @Override
    public void restPassword(int sid, String password) {
        studentDao.updatePasswordBySid(sid,password);
    }
}

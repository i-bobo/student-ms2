package com.yh.service.serviceimpl;

import com.yh.dao.StudentDao;

import com.yh.dao.daoimpl.AdminDaoImpl;
import com.yh.dao.daoimpl.StudentDaoImpl;
import com.yh.dao.daoimpl.TeacherDaoImpl;
import com.yh.javabean.Admin;
import com.yh.javabean.Student;
import com.yh.javabean.Teacher;
import com.yh.service.MainService;

import java.util.ArrayList;

public class MainServiceImpl implements MainService {
    StudentDao studentDao =  new StudentDaoImpl();
    TeacherDaoImpl teacherDao = new TeacherDaoImpl();
    AdminDaoImpl adminDao = new AdminDaoImpl();
    @Override
    public Student studentLogin(String username, String password) {
        ArrayList<Student> students = studentDao.selectByUsernameAndPassword(username, password);
        if(students.isEmpty()){
            return null;
        }
        return students.get(0);
    }

    @Override
    public Teacher teacherLogin(String username, String password) {
        ArrayList<Teacher> teachers = teacherDao.selectByUsernameAndPassword(username, password);
        if(teachers.isEmpty()){
            return null;
        }
        return teachers.get(0);
    }

    @Override
    public Admin AdminLogin(String username, String password) {
        ArrayList<Admin> admins = adminDao.selectByUsernameAndPassword(username, password);
        if(admins.isEmpty()){
            return null;
        }
        return admins.get(0);
    }
}

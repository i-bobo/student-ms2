package com.yh.service.serviceimpl;

import com.yh.dao.StudentDao;

import com.yh.dao.daoimpl.AdminDaoImpl;
import com.yh.dao.daoimpl.StudentDaoImpl;
import com.yh.dao.daoimpl.TeacherDaoImpl;
import com.yh.javabean.Admin;
import com.yh.javabean.Student;
import com.yh.javabean.Teacher;
import com.yh.service.AdminService;

public class AdminServiceImpl implements AdminService {
    StudentDao studentDao =  new StudentDaoImpl();
    TeacherDaoImpl teacherDao = new TeacherDaoImpl();
    AdminDaoImpl adminDao = new AdminDaoImpl();


    @Override
    public void addStudent(String username, String password, String name, int status) {
        Student student = new Student(username, password, name, status);
        studentDao.insert(student);
    }

    @Override
    public void removeStudent(int sid) {
       studentDao.deleteBySid(sid);
    }

    @Override
    public void restStudentPassword(int sid, String password) {
        studentDao.updatePasswordBySid(sid,password);
    }

    @Override
    public void addTeacher(String username, String password, String tname, String tdesc) {
        Teacher teacher = new Teacher(username, password, tname, tdesc);
        teacherDao.insert(teacher);
    }

    @Override
    public void removeTeacher(int tid) {
        teacherDao.deleteByTid(tid);
    }

    @Override
    public void restTeacherPassword(int tid, String password) {
        teacherDao.updatePasswordByTid(tid,password);
    }

    @Override
    public void addAdmin(String username, String password, String desc) {
        Admin admin = new Admin(username, password, desc);
        adminDao.insert(admin);
    }

    @Override
    public void removeAdmin(int id) {
        adminDao.deleteById(id);
    }

    @Override
    public void restAdmintPassword(int id, String password) {
        adminDao.updatePasswordById(id,password);
    }
}

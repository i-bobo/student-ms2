package com.yh.service;

public interface AdminService {

    void addStudent(String username,String password,String name,int status);

    void removeStudent(int sid);

    void restStudentPassword(int sid,String password);

    void addTeacher(String username,String password,String tname,String tdesc);

    void removeTeacher(int tid);

    void restTeacherPassword(int tid,String password);

    void addAdmin(String username,String password,String desc);

    void removeAdmin(int id);

    void restAdmintPassword(int id,String password);

}

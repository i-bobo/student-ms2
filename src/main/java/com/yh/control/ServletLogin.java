package com.yh.control;

import com.yh.javabean.Student;
import com.yh.util.MyJdbcUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletLogin", value = "/ServletLogin")
public class ServletLogin extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("mainView.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String radio = request.getParameter("radio");
        System.out.println(username);
        System.out.println(password);
        System.out.println(radio);
        Cookie tag = new Cookie("tag", "success");
        if("1".equals(radio)){
            String sql = "select * from student where username = ? and password = ?";
            ArrayList<Student> dql = MyJdbcUtil.dql(Student.class, sql, username, password);
            if (dql.size() == 1) {
                Student student = dql.get(0);
                System.out.println(student);
                response.addCookie(tag);
                System.out.println("学生登录成功");
            } else {
                tag.setValue("fail");
                response.addCookie(tag);
                System.out.println("学生登录失败");
            }
        }else if("2".equals(radio)){
            System.out.println("教师登录");
        }else if("3".equals(radio)){
            System.out.println("管理员登录");
        }

    }
}

package com.yh.javabean;

public class Teacher {
    private int tid;
    private String username;
    private String password;
    private String tname; //'教师名称',
    private String tdesc;// '该教师暂无具体信息',

    public Teacher() {
    }

    public Teacher( String username, String password, String tname, String tdesc) {
        this.username = username;
        this.password = password;
        this.tname = tname;
        this.tdesc = tdesc;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTdesc() {
        return tdesc;
    }

    public void setTdesc(String tdesc) {
        this.tdesc = tdesc;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "tid=" + tid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", tname='" + tname + '\'' +
                ", tdesc='" + tdesc + '\'' +
                '}';
    }

}

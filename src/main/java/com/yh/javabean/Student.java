package com.yh.javabean;

public class Student {
    private int sid; //'唯一主键',
    private String username; //'登录账号',
    private String password; // '登录密码',
    private String name;// '学生姓名',
    private Integer tid; // '授课教师id',
    private Double score; // '成绩分数 为null代表授课教师没有登记成绩',
    private Integer status; //'是否被点过名称0-未点 1-被点过',

    public Student() {
    }

    public Student( String username, String password, String name, Integer tid, Double score, Integer status) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.tid = tid;
        this.score = score;
        this.status = status;
    }

    public Student(String username, String password, String name) {
        this.username = username;
        this.password = password;
        this.name = name;
    }

    public Student(String username, String password, String name, Integer status) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.status = status;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", tid=" + tid +
                ", score=" + score +
                ", status=" + status +
                '}';
    }
}
